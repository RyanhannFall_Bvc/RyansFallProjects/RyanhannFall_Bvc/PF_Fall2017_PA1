What problem is this code trying to solve? The code itself is made to track weight loss. You could compare it to a weight loss watch. The code is made to track what day of the week it is, how many steps were taken, and calories were lost.

This is the code befor the fix
The code itself 
// Declare variables 
Declare String dayof week
Declare Integer caloriesLost
Declare Real mileswalked
Display “Enter the day of the week.”
Display “Enter the number of steps reported on the pedometer.”
Input stepstaken 
// Processing
Set mileswalked = stepstaken / 2000
Set caloriesLost = mileswalked * 65
//Output
Display “The following is data for, “ weekday 
Display “ walking”,mileswalked, “ miles results in”, calorieslost, “calories lost.”

Now there are a few errors in this code. Here are the few that I was able to find spot
1 There is only one input when there should be two. The second one should be Input dayofweek
2 You can see on lines 7 and 9 there is a command called “stepstaken” but you will notice that we never Declared that as a command. So any ware between lines 2 and 4 you simply have to add the command “Declare string stepstaken” 
3 Lastly there is a very simple easy to fix error. See on line 12 the line is displaying the data for the week day? Well we never made a command for “weekday” but instead on line 2 we made it “dayofweek”. Simply change weekday and it will work fine.

Here is how the code should look like 

// Declare variables 
Declare String dayof week
Declare Integer caloriesLost
Declare Real mileswalked
Declare string stepstaken
Display “Enter the day of the week.”
Display “Enter the number of steps reported on the pedometer.”
Input stepstaken 
Input dayofweek
// Processing
Set mileswalked = stepstaken / 2000
Set caloriesLost = mileswalked * 65
//Output
Display “The following is data for, “ dayofweek 
Display “ walking”,mileswalked, “ miles results in”, calorieslost, “calories lost.”


And this is my attempt at righting it in python 


#Declare_variables 
     String dayofweek
     Integer caloriesLost
     Real mileswalked
     string stepstaken
     “Enter the day of the week1”
     “Enter the number of steps reported on the pedometer.”
    Input stepstaken 
    Input dayofweek
#Processing
    Set mileswalked = ("stepstaken / 2000")
    Set caloriesLost = ("mileswalked * 65")
#Output
    ("stepstaken / 2000")=
    ("mileswalked * 65")
     “The following is data for, “ dayofweek 
     “ walking”,mileswalked, “ miles results in”, calorieslost, “calories lost

Reflection 
I wont lie This assinment was tougher then the other assinment. That being said this was a good assinment for learning. The errors in the software itself were hard to spot but really easy to fix which is good in a learning assinment, the same goes with the charts as well. The main issue I had was with the python, Im not quite sure how to write in it yet but this did assist a little in learning python.
